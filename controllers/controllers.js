const { validationResult } = require('express-validator');
const db = require('../database/db');
const path = require('path');

const displayHomePage = ('/', (req, res) => {
    res.sendFile(path.join(__basedir + '/views/index.html'));
  });

const initDatabase = ('/init', (req, res) => {

    var sqlQuery =  'CREATE TABLE IF NOT EXISTS users(id INT AUTO_INCREMENT, firstname VARCHAR(50), lastname VARCHAR(50), email VARCHAR(50), PRIMARY KEY(id))';
    db.query(sqlQuery, (err) => {
        if (err) throw err;
    });

    var sqlQuery =  'CREATE TABLE IF NOT EXISTS teams(id INT AUTO_INCREMENT, name VARCHAR(50), PRIMARY KEY(id))';
    db.query(sqlQuery, (err) => {
        if (err) throw err;
    });

    var sqlQuery =  'CREATE TABLE IF NOT EXISTS questions(id INT AUTO_INCREMENT, qnumber INT, qoption CHAR(1), style VARCHAR(2), PRIMARY KEY(id))';
    db.query(sqlQuery, (err) => {
        if (err) throw err;
    });

    var sqlQuery =  'CREATE TABLE IF NOT EXISTS survey_responses(id INT AUTO_INCREMENT, name VARCHAR(100), team_id int, role_id INT, PRIMARY KEY(id))';
    db.query(sqlQuery, (err) => {
        if (err) throw err;
    });

    var sqlQuery =  'CREATE TABLE IF NOT EXISTS question_responses(id INT AUTO_INCREMENT, survey_response_id INT, qnumber INT, response CHAR(1), PRIMARY KEY(id), FOREIGN KEY(survey_response_id))';
    db.query(sqlQuery, (err) => {
        if (err) throw err;
    });

    res.send('Database initialised');

});

const getUsers = (req, res) => {
    const sqlQuery = 'SELECT * FROM users';
  
    db.query(sqlQuery, (err, result) => {
      if (err) {
        res.json({'error': err.sqlMessage});
      }
      else {
        res.json({'users': result});
      }
    });
  };

  const addUser = (req, res) => {
    const errors = validationResult(req);

    if (errors.array().length > 0) {
        res.send(errors.array());
    } else {
        const user = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email
        };

        const sqlQuery = 'INSERT INTO users SET ?';

        db.query(sqlQuery, user, (err, row) => {
            if (err) throw err;

            res.send('User added successfully!');
        });
    }
};

const getTeams = (req, res) => {
  const sqlQuery = 'SELECT * FROM teams';
  
  db.query(sqlQuery, (err, result) => {
    if (err) {
      res.json({'error': err.sqlMessage});
    }
    else {
      res.json({'teams': result});
    }
  });
};

const getQuestions = (req, res) => {
  const sqlQuery = 'SELECT * FROM questions';
  
  db.query(sqlQuery, (err, result) => {
    if (err) {
      res.json({'error': err.sqlMessage});
    }
    else {
      res.json({'questions': result});
    }
  });
};

const getSurveyResponses = (req, res) => {
  const sqlQuery = 'SELECT s.id, s.name, t.name team, role_id FROM survey_responses s, teams t where s.team_id = t.id';
  
  db.query(sqlQuery, (err, result) => {
    if (err) {
      res.json({'error': err.sqlMessage});
    }
    else {
      res.json({'survey_responses': result});
    }
  });
};

const takeSurvey = (req, res) => {
  res.send('Take Survey not implemented yet!');
};

const showResults = (req, res) => {
  res.send('Survey Results not implemented yet!');
};

module.exports = {
    displayHomePage,
    initDatabase,
    getUsers,
    addUser,
    getTeams,
    getQuestions,
    getSurveyResponses,
    takeSurvey,
    showResults
  }