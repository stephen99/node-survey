'use strict';

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

const express = require('express');
const app = express();

const routes = require('./routes/routes');

global.__basedir = __dirname;

app.use(express.json());
app.use(routes);

app.listen(PORT, HOST);

console.log(`Running on http://${HOST}:${PORT}`);