FROM node:latest
ENV NODE_ENV=dev
WORKDIR /usr/app
COPY ["package*.json", "./"]
RUN npm install --include=dev
COPY . .