const express = require('express');
const controller = require('../controllers/controllers');
const { body } = require('express-validator');
const router = new express.Router();

router.get('/', controller.displayHomePage);

router.get('/init', controller.initDatabase);

router.get('/users', controller.getUsers);
router.post('/users',
    body('firstname').not().isEmpty().escape(),
    body('lastname').not().isEmpty().escape(),
    body('email').isEmail().normalizeEmail(),
    controller.addUser
);

router.get('/teams', controller.getTeams);

router.get('/questions', controller.getQuestions);

router.get('/survey', controller.takeSurvey);

router.get('/results', controller.getSurveyResponses);

module.exports = router;