const mysql = require('mysql2');

require('dotenv').config();

const database = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_ROOT_PASSWORD,
    database: process.env.DB_DATABASE
});

database.connect(function(err) {
    if (err) throw err;
});

module.exports = database;